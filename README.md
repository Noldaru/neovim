# NeoVim Configuration

This repository contains a comprehensive configuration for NeoVim, complete with a variety of commonly used extensions.

## Table Of Contents

- [NeoVim Configuration](#neovim-configuration)
  - [Table Of Contents](#table-of-contents)
  - [Installation](#installation)
    - [Script Requirements](#script-requirements)

## Installation

To install, simply execute the `install` script located at the root of this repository.

### Script Requirements

The installation script is written in Python and requires Python version `3.6` or higher.

The script supports the following operating systems:

- Linux
  - CentOS
  - Rocky Linux
  - Ubuntu
- MacOS
- Windows

Please note that for `CentOS versions earlier than 8`, `Ubuntu versions earlier than 18.04`, `MacOS`, and `Windows`, you will need to manually install Python 3.6 or a later version. The later versions of the mentioned <ins>Linux distributions</ins> (and any version of Rocky Linux) come with Python 3.6 or a higher version pre-installed.

For guidance on how to install Python 3.6 or later on your operating system, please refer to the appropriate online resources.
